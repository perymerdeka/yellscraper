# import module
import codecs
import csv
import glob
import io
import json
import os
import re
import time

import gspread
import pandas as pd
import requests
from bs4 import BeautifulSoup
from flask import Blueprint, redirect, url_for, render_template, request, send_from_directory, Response
# configure blueprint
# from project import app
from sqlalchemy import text

from project import db
from project.models.models import Records

# blueprint
scraper_blueprint = Blueprint('scraper', __name__, template_folder='templates')

# data site
base_url = 'https://www.yell.com'

# headers
headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.92 Safari/537.36'
}


# get total pages function
def get_total_page(keyword, location):
    params = {
        'keywords': keyword,
        'location': location,
        'scrambleSeed': '1189397696',
    }

    # url site
    url = 'https://www.yell.com/ucs/UcsSearchAction.do?'
    req = requests.get(url=url, params=params, headers=headers)
    soup = BeautifulSoup(req.text, 'html.parser')
    try:
        pagination = soup.find('nav', attrs={'class': 'row'}).findAll('a', attrs={'class': 'btn'})
    except:
        pagination = soup.find('nav', attrs={'class': 'row pagination'}).findAll('a', attrs={'class': 'btn'})

    total = len(pagination)
    return total


# function to get all url per page
def get_url_per_page(page, keyword, location):
    data_link = []
    params = {
        'keywords': keyword,
        'location': location,
        'pageNum': page

    }

    url = 'https://www.yell.com/ucs/UcsSearchAction.do?'
    req = requests.get(url, params=params, headers=headers)
    soup = BeautifulSoup(req.text, 'html.parser')
    product_Link = soup.findAll('div', attrs={'class': 'row businessCapsule--mainRow'})
    for item in product_Link:
        link = item.find('div', attrs={'class': 'businessCapsule--titSpons'}).find('a')['href']
        data_link.append(link)

    return data_link


# add counter to rotating vpn if IP Blocked
counter = 0


def get_detail(url):
    global counter, address, city, postal_code

    # request url
    req = requests.get(base_url + url, headers=headers)

    # handling error with html
    f = open(file='./project/scraper/temp/test.html', mode='w+')
    f.write(req.text)

    # read for html in temp directory
    reader = codecs.open(filename='./project/scraper/temp/test.html', mode='r').read()
    parser = BeautifulSoup(reader, 'html.parser')
    try:
        title_text = parser.find('title').text.strip()
    except:
        title_text = parser.find('h1', attrs={'class':'text-h1 businessCard--businessName'})
    # cheking if error
    while 'Are you human?' in title_text:
        with open(file='./project/scraper/server_list/vpn.txt') as data_server:
            vpn_list = data_server.readlines()
            server_list = []
            for vpn in vpn_list:
                server_list.append(vpn.strip())

            # rotating vpn
            try:
                print('Try VPN Server... {}'.format(server_list[counter]))
                os.system('windscribe connect {}'.format(server_list[counter]))
                counter += 1
                break
            except UnboundLocalError:
                continue

    print('Getting Detail URL: {}'.format(url))

    # request URL
    req = requests.get(base_url + url, headers=headers)

    # parsing content from site
    soup = BeautifulSoup(req.text, 'html.parser')

    # get content
    try:
        title = soup.find('h1', attrs={'class': 'text-h1 businessCard--businessName'}).text.strip()
    except:
        title = soup.find('div', attrs={'class': 'text-h1 businessCard--businessName'})

    more_info = soup.findAll('span', attrs={'class': 'address'})
    for info in more_info:
        address = info.find('span', attrs={'itemprop': 'streetAddress'}).text.strip()
        city = info.find('span', attrs={'itemprop': 'addressLocality'}).text.strip()
        postal_code = info.find('span', attrs={'itemprop': 'postalCode'}).text.strip()

    try:
        website = soup.find('a', attrs={'class': 'btn btn-big btn-yellow businessCard--callToAction'})['href']
        telp = soup.find('span', attrs={'class': 'business--telephoneNumber'}).text.strip()
    except:
        website = 'No Website'
        telp = 'No Telephone'

    data_detail = {
        'title': title,
        'address': address,
        'city': city,
        'postal Code': postal_code,
        'telp': telp,
        'website': website
    }

    return data_detail


# route for scraping
@scraper_blueprint.route('/get-url', methods=['GET', 'POST'])
def get_url():
    if request.method == 'POST':
        keyword = request.form['keyword']
        location = request.form['location']

        total_pages = get_total_page(keyword=keyword, location=location)

        data_link = []
        for page in range(total_pages):
            page += 1
            url_per_page = get_url_per_page(keyword=keyword, location=location, page=page)
            data_link += url_per_page

        # extract the url
        result = []
        for url in data_link:
            result.append(url)

        url_count = len(result)

        # get all url
        data_url = {
            'url_list': result
        }

        with open('./project/scraper/result/urls.json', 'w') as url_json:
            json.dump(data_url, url_json)
        print('json generated')
        return render_template('index.html', datas=data_url['url_list'], url_count=url_count)
    return render_template('index.html')


# helper to create to csv and sorted json file
def add_to_sheet():
    gc = gspread.service_account(filename='credentials.json')
    sheets = gc.open_by_key('12ImQ4M4IGYLaQFZY8gRDQcF7Lb_dB99VwsWbv3qTUhQ')
    # read worksheet on drive
    worksheet = sheets.sheet1  # why sheet1 because in this case we only have one sheet
    scrap_res = sorted(glob.glob('./project/scraper/result/downloaded_json/*.json'), key=lambda x: float(re.findall("(\d)", x)[0]))
    scrap_datas = []
    for scrap in scrap_res:
        print('generating JSON: ', scrap)
        with open(scrap) as json_file:

            datas = json.load(json_file)
        scrap_datas.append(datas)

    df = pd.DataFrame(scrap_datas)
    worksheet.update([df.columns.values.tolist()] + df.values.tolist())
    print('Upload Complete')



@scraper_blueprint.route('/view_data')
def view_data():
    records = Records.query.all()

    return render_template('view_data.html', records=records)


# create route to generated json for detail page
@scraper_blueprint.route('/generate_json', methods=['GET', 'POST'])
def generate_json():
    if request.method == 'POST':
        start = request.form['start_list']
        endlist = request.form['end_list']

        start = int(start)
        endlist = int(endlist)

        # read json file from urls,json in result folder
        with open('./project/scraper/result/urls.json', 'r') as read_json:
            datas = json.load(read_json)

        # get input for start generate josn file in download_jon directory
        urls = datas['url_list']
        urls = urls[start:endlist]

        # loop for get file generated json
        for id, url, in enumerate(urls):
            time.sleep(1)
            print('Generated Json File: {} of {}'.format(start + id + 1, endlist))
            data_dict = get_detail(url)

            # generated json file then save to directory
            with open('./project/scraper/result/downloaded_json/{}.json'.format(start + id + 1), 'w') as files:
                json.dump(data_dict, files)
            print('generated json complete')

            # display list with sorteed json
            files = sorted(glob.glob('./project/scraper/result/downloaded_json/*.json'),
                           key=lambda x: float(re.findall("(\d)", x)[0]))
            all_datas = []
            for file in files:
                # read file
                print('sorted file json ', file)
                with open(file) as json_file:
                    datas = json.load(json_file)
                all_datas.append(datas)

            # data dictionary
            url_data = {
                'data_url':all_datas
            }

            # save to database
            try:
                db.session.execute(text(
                    'TRUNCATE TABLE records RESTART IDENTITY;'
                ))


                for value in url_data['data_url']:
                    title = value['title']
                    address = value['address']
                    city = value['city']
                    postal_code = value['postal Code']
                    telp = value['telp']
                    website = value['website']

                    # save to database
                    new_records = Records(title=title, address=address, city=city, postal_code=postal_code, telp=telp, website=website)
                    db.session.add(new_records)
                    db.session.commit()

                add_to_sheet()


            except Exception:
                db.session.rollback()







            # drop table before save to database
            # try:
            #     db.session.execute(
            #         text('TRUNCATE TABLE records RESTART IDENTITY;')
            #     )
            #
            #     for value in values:
            #         title = value['title']
            #         address = value['address']
            #         city = value['city']
            #         postal_code = value['postal Code']
            #         telp = value['telp']
            #         website = value['website']
            #
            #         # save to database
            #         new_records = Records(title=title, address=address, city=city, postal_code=postal_code, telp=telp,
            #                               website=website)
            #         db.session.add(new_records)
            #         db.session.commit()
            #
            #     add_to_sheet()
            #
            # except:
            #     db.session.rollback()
        return redirect(url_for('scraper.view_data'))

    return render_template('generate_json.html')


# route to download csv
@scraper_blueprint.route('/csv')
def download_csv():
    output = io.StringIO()
    writer = csv.writer(output)

    # headers
    header = ['title', 'address', 'city', 'postal Code', 'telp', 'website']
    writer.writerow(header)

    # read from database
    records = Records.query.all()
    for record in records:
        rows = [record.title, record.address, record.city, record.postal_code, record.telp, record.website]
        writer.writerow(rows)

    output.seek(0)
    return Response(output, mimetype="text/csv", headers={"Content-Disposition":"attachment; filename=yell-data.csv"})


# helper for display name json
@scraper_blueprint.route('/result/downloaded_json<filename>')
def display_name(filename):
    return send_from_directory('./project/scraper/result/downloaded_json', filename)
