import glob
import json
import re
import pandas as pd
import gspread


def add_to_sheet():
    gc = gspread.service_account(filename='credentials.json')
    sheets = gc.open_by_key('12ImQ4M4IGYLaQFZY8gRDQcF7Lb_dB99VwsWbv3qTUhQ')
    # read worksheet on drive
    worksheet = sheets.sheet1  # why sheet1 because in this case we only have one sheet
    scrap_res = sorted(glob.glob('./project/scraper/result/downloaded_json/*.json'), key=lambda x: float(re.findall("(\d)", x)[0]))
    scrap_datas = []
    for scrap in scrap_res:
        print('generating JSON: ', scrap)
        with open(scrap) as json_file:

            datas = json.load(json_file)
        scrap_datas.append(datas)

    df = pd.DataFrame(scrap_datas)
    worksheet.update([df.columns.values.tolist()] + df.values.tolist())
    print('Upload Complete')


add_to_sheet()