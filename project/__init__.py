# import module
import os
from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

# inisiate of extension

# database
db = SQLAlchemy()

# migration
migrate = Migrate()

# create function to create App
def create_app(script_info=None):

    # import module for scraper blueprint and models
    from project.models import models
    from project.scraper.views import scraper_blueprint
    from project.home.views import home_blueprint

    # inisiate app
    app = Flask(__name__, static_url_path='')

    # setting config ..?
    app.config.from_object('config')

    # setup extenstion
    db.init_app(app=app)
    migrate.init_app(app=app, db=db)

    # register Blueprint
    app.register_blueprint(blueprint=home_blueprint, url_prefix='/')
    app.register_blueprint(blueprint=scraper_blueprint)

    # shell contect from flask cli
    @app.shell_context_processor
    def ctx():
        return {'app':app, 'db':db}

    # returning aplication
    return app
