# models database config

# import module here
import json
from project import db

# create class to define our model
class Records(db.Model):
    __tablename__='records'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(), nullable=False)
    address = db.Column(db.String(), nullable=False)
    city = db.Column(db.String(), nullable=False)
    postal_code = db.Column(db.String(), nullable=False)
    telp = db.Column(db.String(), nullable=False)
    website = db.Column(db.String(), nullable=False)

    def __init__(self, title, address, city, postal_code, telp, website):
        self.title = title
        self.address = address
        self.city = city
        self.postal_code = postal_code
        self.telp = telp
        self.website = website

    def __repr__(self):
        return "Record Data {}".format(self.title)