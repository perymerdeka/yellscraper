# import module
import os

# set path project
BASE_DIR = os.path.abspath(os.path.dirname(__file__))
TOP_LEVEL_DIR = os.path.abspath(os.curdir)

# Setup Flask App
SECRET_KEY = 'this_is_secret_key'
DEBUG = True

# Config Database Sqlalchemy

# URL Database for development
# SQLALCHEMY_DATABASE_URI = 'postgresql://mimin:root@localhost/yellscraper_db'
# URL Database for Production (heroku)
SQLALCHEMY_DATABASE_URI = 'postgres://fopmtggpyybxnq:a373f281cce3b6201df8c7682eeb0dfc1a0a59862c0232d92c735eae93f6d180@ec2-54-157-234-29.compute-1.amazonaws.com:5432/d5ae3c932pukfi'

SQLALCHEMY_TRACK_MODIFICATIONS = True
SQLALCHEMY_ECHO = False


# Listing directory downloaded_json
JSON_DIRECTORY = TOP_LEVEL_DIR + 'project/scraper/result/downloaded_json'

